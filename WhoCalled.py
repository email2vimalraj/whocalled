import os
import re
import json
from time import time
from threading import Thread
import sublime
import sublime_plugin

class WhoCalledCommand(sublime_plugin.ApplicationCommand):
  """
  Application command class for Sublime Text.
  """

  def __init__(self):
    """
    Python init medthod.
    """
    sublime.set_timeout(self._on_init, 1000)

  def _on_init(self):
    """
    Initializes necessary functionality and kicks off indexer.
    """
    # Running Sublime Text 2?
    self.is_st2 = int(sublime.version()) < 3000

    # Import libs for WhoCalled.
    if self.is_st2:
      import WhoCalledUtil
    else:
      from . import WhoCalledUtil

    # Define helper classes.
    self.util = WhoCalledUtil.util()
    self.indexer = WhoCalledUtil.indexer()

    # Get paths.
    self.paths = self.util.get_paths()
    # Get settings.
    self.settings = self.util.get_settings()

    # Run indexer.
    if self.is_st2:
      if self.settings['always_force_indexing']:
        Thread(target = self.run_indexer, args = (True,)).start()
      else:
        Thread(target = self.run_indexer, args = (False,)).start()
    else:
      if self.settings['always_force_indexing']:
        sublime.set_timeout_async(self.run_forced_indexer, 1)
      else:
        sublime.set_timeout_async(self.run_indexer, 1)

  def run_forced_indexer(self, description = None):
    """
    Callback to asynchronous timout for starting the indexer.
    """
    self.run_indexer(True)

  def run_indexer(self, force_indexing = False):
    """
    Starts the indexer.
    """
    # Create paths if necessary.
    if not os.path.isdir(self.paths['user_data_index']):
      os.makedirs(self.paths['user_data_index'])

    # Make sure that timestamp file exists.
    last_indexing_timestamp_file = os.path.join(self.paths['user_data'], 'last_indexing_timestamp')
    if not os.path.exists(last_indexing_timestamp_file):
      with open(last_indexing_timestamp_file, 'w') as timestamp_file:
        timestamp_file.write(str(time()))
        force_indexing = True

    # Start indexing.
    sublime.set_timeout(self._indexing_start_message, 1)
    project_folders = self.util.get_project_folders()
    for folder in project_folders:
      if force_indexing:
        self.indexer.index_files(folder)
      else:
        self.indexer.re_index_files(folder)
    sublime.set_timeout(self._indexing_done_message, 1)

    # Update timestamp file.
    with open(last_indexing_timestamp_file, 'w') as timestamp_file:
      timestamp_file.write(str(time()))

  def _indexing_start_message(self):
    """
    Status message.
    """
    sublime.status_message('WhoCalled says: "Function indexing started."')

  def _indexing_done_message(self):
    """
    Status message.
    """
    sublime.status_message('WhoCalled says: "Function indexing done, good to go."')

  def _add_remove_project_folders(self, idx = 0):
    """
    Shows quick panel for adding or removing folders to index.
    """
    # Get project folders.
    all_project_folders = self.util.get_all_project_folders()
    active_project_folders = self.util.get_project_folders()

    # Add folder name and path to index list.
    self.new_index_folders = []
    for path in all_project_folders:
      if path in active_project_folders:
        self.new_index_folders.append(['* ' + os.path.basename(path), path])
      else:
        self.new_index_folders.append([os.path.basename(path), path])

    self.window.show_quick_panel(self.new_index_folders, self._on_select_project_folder, 0, idx)

  def _on_select_project_folder(self, idx):
    """
    Callback to quick panel selection. Stores or removes selected folder to/from JSON.
    """
    if -1 < idx:
      # Get project folders.
      project_folders = self.util.get_project_folders()

      # Add or remove folders from index list.
      if self.new_index_folders[idx][1] in project_folders:
        project_folders.remove(self.new_index_folders[idx][1])
      else:
        project_folders.append(self.new_index_folders[idx][1])

      # Save index file.
      with open(os.path.join(self.paths['user_data'], 'index_folders.json'), 'w') as file:
        file.write(json.dumps(project_folders, sort_keys = False, indent = 2, separators = (',', ': ')))

      # Set a short timeout to let the previous quick panel close and allow a new one to open.
      sublime.set_timeout(lambda: self._add_remove_project_folders(idx), 10)

  def run(self, **kwargs):
    """
    Sublime Text main method.
    """
    # Set wiew and window for effiency.
    self.window = sublime.active_window()
    view = self.window.active_view()

    # Special modes run from the regular command palette.
    if 'folders' == kwargs['mode']:
      self._add_remove_project_folders()
      return
    if 'index' == kwargs['mode']:
      if self.is_st2:
        Thread(target = self.run_forced_indexer, args = ('run_forced_indexer',)).start()
      else:
        sublime.set_timeout_async(self.run_forced_indexer, 1)
      return

    # Get mode ("def" or "call" lookup).
    self.mode = kwargs['mode']

    # Load syntaxes and extract file extension.
    syntaxes = self.settings['syntaxes']
    file_extension = os.path.splitext(view.file_name())[1][1:]

    if file_extension in syntaxes:
      # Select the region where the cursor is placed and extract the content from it.
      selection = view.sel()[0]
      # Get syntax.
      syntax = syntaxes[file_extension]

      # Container for functions.
      self.functions = []

      # If selected word is a function.
      if '(' == view.substr(view.word(selection).end()):
        self.functions.append(view.substr(view.word(selection)))
      else:
        content = view.substr(view.line(selection))
        match = re.search(syntax['uni'], content)
        while match:
          # Grab new functions as long as there are any on the same line.
          self.functions.append(match.group())
          content = content[:match.start()] + content[match.end():]
          match = re.search(syntax['uni'], content)

      if 0 == len(self.functions):
        sublime.status_message('WhoCalled says: "No function found."')
      elif 1 == len(self.functions):
        self._do_lookup(self.functions[0])
      else:
        self.window.show_quick_panel(self.functions, self._pre_do_lookup, 0)
    else:
      sublime.status_message('WhoCalled says: "Programming language not supported."')

  def _pre_do_lookup(self, idx):
    """
    Prepares for looking for function in index to show in quick panel.
    """
    # Set a short timeout to let the previous quick panel close and allow a new one to open.
    sublime.set_timeout(lambda: self._do_lookup(self.functions[idx]), 10)

  def _do_lookup(self, function):
    """
    Looks for function in index to show in quick panel.
    """
    # Init function buckets.
    self.function_paths = []
    self.function_lines = []

    # Whether to look for a definition or a call.
    file_name_prefix = self.mode + '_'

    # Get folder path and name from current project folder.
    folder_path = self.util.get_current_project_folder(True)
    folder_name = os.path.basename(folder_path)

    # Load indes file for the corresponding project folder.
    function_index_file = os.path.join(self.paths['user_data_index'], file_name_prefix + folder_name.replace(' ', '_') + '.json')

    if os.path.exists(function_index_file):
      # Load the function index from file.
      function_index = self.util.parse_json_file(function_index_file)

      # Set paths, lines and quick panel function items for the selected function.
      self.function_items = []
      for path in function_index:
        if function in function_index[path]:
          for line_number in function_index[path][function]:
            self.function_paths.append(path)
            self.function_lines.append(line_number)
            self.function_items.append([os.path.basename(path) + ' (' + line_number + ')', 'Path: ' + os.path.join(folder_name, path.replace(folder_path + os.sep, ''))])

      if self.function_items:
        # Show quick panel with added function items.
        if self.is_st2:
          self.window.show_quick_panel(self.function_items, self._on_highlight, 0, 0)
        else:
          self.window.show_quick_panel(self.function_items, self._on_select, 0, 0, self._on_highlight)
      else:
        sublime.status_message('WhoCalled says: "There is no matching function in the current project folder."')
    else:
      sublime.status_message('WhoCalled says: "There is no index file for the current project folder."')

  def _on_highlight(self, idx):
    """
    Callback to quick panel selection, when highlighting a function item.
    """
    # Get view from opened file.
    view = sublime.active_window().open_file(self.function_paths[idx], sublime.TRANSIENT)

    # When the view has loaded, show it.
    self.util.do_when(
      lambda: not view.is_loading(),
      lambda: self._show_view(view, idx)
    )

  def _on_select(self, idx):
    """
    Callback to quick panel selection, when selecting a function item.
    """
    # Do nothing.
    return

  def _show_view(self, view, idx):
    """
    Shows view with highlighted row on selected function item's row.
    """
    # Create a point from the selected line item.
    point = view.text_point(int(self.function_lines[idx]) - 1, 0)

    # Create a whole line from point.
    line = view.line(point)

    # Add region to selection and show it.
    view.sel().clear()
    view.sel().add(line)
    view.show_at_center(point)

    if self.is_st2:
      if -1 < idx:
        sublime.active_window().show_quick_panel(self.function_items, self._on_highlight, 0, idx)